﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpHomework7Module14
{
    public class Cards
    {
        int[,] cardsArray = new int[,]
        {
        { 6,7,8,9,10,11,12,13,14 },
        { 6,7,8,9,10,11,12,13,14 },
        { 6,7,8,9,10,11,12,13,14 },
        { 6,7,8,9,10,11,12,13,14 }
        };

        Dictionary<string, int> dimond6;
        Dictionary<string, int> dimond7;
        Dictionary<string, int> dimond8;
        Dictionary<string, int> dimond9;
        Dictionary<string, int> dimond10;
        Dictionary<string, int> dimond11;
        Dictionary<string, int> dimond12;
        Dictionary<string, int> dimond13;
        Dictionary<string, int> dimond14;

        Dictionary<string, int> spear6;
        Dictionary<string, int> spear7;
        Dictionary<string, int> spear8;
        Dictionary<string, int> spear9;
        Dictionary<string, int> spear10;
        Dictionary<string, int> spear11;
        Dictionary<string, int> spear12;
        Dictionary<string, int> spear13;
        Dictionary<string, int> spear14;

        Dictionary<string, int> club6;
        Dictionary<string, int> club7;
        Dictionary<string, int> club8;
        Dictionary<string, int> club9;
        Dictionary<string, int> club10;
        Dictionary<string, int> club11;
        Dictionary<string, int> club12;
        Dictionary<string, int> club13;
        Dictionary<string, int> club14;

        Dictionary<string, int> heart6;
        Dictionary<string, int> heart7;
        Dictionary<string, int> heart8;
        Dictionary<string, int> heart9;
        Dictionary<string, int> heart10;
        Dictionary<string, int> heart11;
        Dictionary<string, int> heart12;
        Dictionary<string, int> heart13;
        Dictionary<string, int> heart14;

        public Stack<Dictionary<string, int>> deckStack = new Stack<Dictionary<string, int>>();


        public Cards()
        {
            dimond6 = new Dictionary<string, int> { { "♦", 6 } };
            dimond7 = new Dictionary<string, int> { { "♦", 7 } };
            dimond8 = new Dictionary<string, int> { { "♦", 8 } };
            dimond9 = new Dictionary<string, int> { { "♦", 9 } };
            dimond10 = new Dictionary<string, int> { { "♦", 10 } };
            dimond11 = new Dictionary<string, int> { { "♦", 11 } };
            dimond12 = new Dictionary<string, int> { { "♦", 12 } };
            dimond13 = new Dictionary<string, int> { { "♦", 13 } };
            dimond14 = new Dictionary<string, int> { { "♦", 14 } };

            spear6 = new Dictionary<string, int> { { "♠", 6 } };
            spear7 = new Dictionary<string, int> { { "♠", 7 } };
            spear8 = new Dictionary<string, int> { { "♠", 8 } };
            spear9 = new Dictionary<string, int> { { "♠", 9 } };
            spear10 = new Dictionary<string, int> { { "♠", 10 } };
            spear11 = new Dictionary<string, int> { { "♠", 11 } };
            spear12 = new Dictionary<string, int> { { "♠", 12 } };
            spear13 = new Dictionary<string, int> { { "♠", 13 } };
            spear14 = new Dictionary<string, int> { { "♠", 14 } };

            club6 = new Dictionary<string, int> { { "♣", 6 } };
            club7 = new Dictionary<string, int> { { "♣", 7 } };
            club8 = new Dictionary<string, int> { { "♣", 8 } };
            club9 = new Dictionary<string, int> { { "♣", 9 } };
            club10 = new Dictionary<string, int> { { "♣", 10 } };
            club11 = new Dictionary<string, int> { { "♣", 11 } };
            club12 = new Dictionary<string, int> { { "♣", 12 } };
            club13 = new Dictionary<string, int> { { "♣", 13 } };
            club14 = new Dictionary<string, int> { { "♣", 14 } };

            heart6 = new Dictionary<string, int> { { "♥", 6 } };
            heart7 = new Dictionary<string, int> { { "♥", 7 } };
            heart8 = new Dictionary<string, int> { { "♥", 8 } };
            heart9 = new Dictionary<string, int> { { "♥", 9 } };
            heart10 = new Dictionary<string, int> { { "♥", 10 } };
            heart11 = new Dictionary<string, int> { { "♥", 11 } };
            heart12 = new Dictionary<string, int> { { "♥", 12 } };
            heart13 = new Dictionary<string, int> { { "♥", 13 } };
            heart14 = new Dictionary<string, int> { { "♥", 14 } };

            deckStack = new Stack<Dictionary<string, int>>();

            deckStack.Push(dimond6);
            deckStack.Push(dimond7);
            deckStack.Push(dimond8);
            deckStack.Push(dimond9);
            deckStack.Push(dimond10);
            deckStack.Push(dimond11);
            deckStack.Push(dimond12);
            deckStack.Push(dimond13);
            deckStack.Push(dimond14);

            deckStack.Push(spear6);
            deckStack.Push(spear7);
            deckStack.Push(spear8);
            deckStack.Push(spear9);
            deckStack.Push(spear10);
            deckStack.Push(spear11);
            deckStack.Push(spear12);
            deckStack.Push(spear13);
            deckStack.Push(spear14);

            deckStack.Push(club6);
            deckStack.Push(club7);
            deckStack.Push(club8);
            deckStack.Push(club9);
            deckStack.Push(club10);
            deckStack.Push(club11);
            deckStack.Push(club12);
            deckStack.Push(club13);
            deckStack.Push(club14);

            deckStack.Push(heart6);
            deckStack.Push(heart7);
            deckStack.Push(heart8);
            deckStack.Push(heart9);
            deckStack.Push(heart10);
            deckStack.Push(heart11);
            deckStack.Push(heart12);
            deckStack.Push(heart13);
            deckStack.Push(heart14);

        }



    }
}







