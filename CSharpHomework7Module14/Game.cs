﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpHomework7Module14
{
    public class Game
    {
        Random random;

        Cards deck;

        public Game()
        {
            random = new Random();

            deck = new Cards();
        }

        public Stack<Dictionary<string, int>> Shuffle()
        {
            return new Stack<Dictionary<string, int>>(deck.deckStack.OrderBy(x => random.Next()));
        }

        public void Cards_distributiuon(Player player1, Player player2)
        {
            var shuffledDeck = Shuffle();
            while (shuffledDeck.Count != 0)
            {
                player1.myStack.Push(shuffledDeck.Pop());
                player2.myStack.Push(shuffledDeck.Pop());

            }


        }

    }
}
