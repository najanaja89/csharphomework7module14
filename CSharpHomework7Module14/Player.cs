﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpHomework7Module14
{
    public class Player
    {
        Random random;

        public string Name { private set; get; }
        public int Count { set; get; }
        public Stack<Dictionary<string, int>> myStack;


        public Player(string name)
        {
            random = new Random();
            Name = name;
            Count = 0;
            myStack = new Stack<Dictionary<string, int>>();

        }

        public int GetCardValue()
        {
            if (myStack != null)
            {
                foreach (var item in myStack)
                {
                    foreach (var i in item)
                    {
                        return i.Value;
                    }
                }
                return 0;
            }
            else return 0;
        }


        public string GetCardSuit()
        {
            if (myStack != null)
            {
                foreach (var item in myStack)
                {
                    foreach (var i in item)
                    {
                        return i.Key;
                    }
                }
                return "null";
            }
            else return "null";
        }

        public void ShowMyStack()
        {
            foreach (var item in myStack)
            {
                foreach (var i in item)
                {
                    Console.Write(i.Value + "\t");
                }
            }
            Console.WriteLine();
            Console.WriteLine("--------------------------------------------");
        }

        private Stack<Dictionary<string, int>> Shuffle()
        {
            return new Stack<Dictionary<string, int>>(myStack.OrderBy(x => random.Next()));
        }

        public void MyStackShuffle()
        {
            Stack<Dictionary<string, int>> temp = new Stack<Dictionary<string, int>>();
            temp = Shuffle();
            myStack = temp;
        }
    }
}
