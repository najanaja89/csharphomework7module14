﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpHomework7Module14
{
    class Program
    {
        static void Main(string[] args)
        {


            Player player1 = new Player("Player 1");
            Player player2 = new Player("Player 2");
            int step = 0;
            Stack<Dictionary<string, int>> temp = new Stack<Dictionary<string, int>>();

            Cards cards = new Cards();
            Game game = new Game();

            Stack<Dictionary<string, int>> Shuffled = new Stack<Dictionary<string, int>>();
            Shuffled = game.Shuffle();

            game.Cards_distributiuon(player1, player2);


            while (true)
            {
                if (player1.myStack.Count == 0 || player1.myStack.Count < 7)
                {

                    System.Windows.Forms.MessageBox.Show("Player 2 win!");
                    Environment.Exit(0);
                }

                else if (player2.myStack.Count == 0 || player2.myStack.Count < 7)
                {
                    System.Windows.Forms.MessageBox.Show("Player 1 win!");
                    Environment.Exit(0);
                }

                if (player1.GetCardValue() > player2.GetCardValue())
                {
                    var tmp = new Stack<Dictionary<string, int>>(player1.myStack);
                    player1.myStack = tmp;
                    player1.myStack.Push(player2.myStack.Pop());
                    tmp = new Stack<Dictionary<string, int>>(player1.myStack);
                    player1.myStack = tmp;
                }
                else if (player1.GetCardValue() < player2.GetCardValue())
                {
                    var tmp = new Stack<Dictionary<string, int>>(player2.myStack);
                    player2.myStack = tmp;
                    player2.myStack.Push(player1.myStack.Pop());
                    tmp = new Stack<Dictionary<string, int>>(player2.myStack);
                    player2.myStack = tmp;
                }

                else
                {
                    player1.MyStackShuffle();
                }


                step++;
                Console.WriteLine("step " + step);
            }



            Console.ReadLine();
        }
    }
}
